# Intevation - Schulung Go

* 10.11 - 12.11.2020 KW 46


* [Agenda](agenda.md)
* [Links](links.md)
* [Code-Beispiele](https://gitlab.com/sascha.l.teichmann/go-examples)
* [Live-Coding-Beispiele](workspace/src/README.md)
