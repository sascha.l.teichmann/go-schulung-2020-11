# Bausteine

## Tag 1

### Go - Allgemein

- Was ist Go?

### Go - Ökosystem

- Community
- Social Coding
- Libraries

### I - Basics (Teil 1)

- Pakete / Module / Programmstruktur, Kommentare
- Funktionen und Aufrufe: `func`, Call by value
    - init-Funktionen
- Kontrollstrukturen und Syntax:
    - "Oberon mit C-Syntax" [Evolution of Go (Robert Griesemer)](https://talks.golang.org/2015/gophercon-goevolution.slide)
    - `if`, `for`, `switch`, `goto`
- Operatoren
- Datenstrukturen:
    - Variablen `var`:
      - einfache: `int`, `float32`, `rune`, ...
          - [Numeric types](https://golang.org/ref/spec#Numeric_types)
      - zusammengesetzte:
          - Structs und Arrays
      - gebundene:
          - Slices
              - [SliceTricks](https://github.com/golang/go/wiki/SliceTricks)
          - Maps
              - [Presentation: Inside the Map Implementation (Keith Randall)](https://docs.google.com/presentation/d/1CxamWsvHReswNZc7N2HMV7WPFqS8pvlPVZcDegdC_T4/edit#slide=id.p)
              - [Video: Inside the Map Implementation (Keith Randall)](https://www.youtube.com/watch?v=Tl7mi9QmLns)
        - Funktionen II / Closures / [Variadische Funktionen](https://de.wikipedia.org/wiki/Variadische_Funktion)
            - Bsp. für variadische Funktion [func Printf aus dem fmt Package](https://golang.org/pkg/fmt/#Printf)
        - (Channels -> Tag 2)
      - Kontrollstrukturen II: `range`
      - Pointer
- Builtin-Funktionen: `append`, `copy`, `len`, `cap`, ...
- `defer`, `panic`, `recover`
    - [Beispielaufgabe](https://gitlab.com/sascha.l.teichmann/go-examples/-/blob/master/methodexp/main.go)
- `return`: Tuple und Fehlerbehandlung
    - [Errors are values (Rob Pike)](https://blog.golang.org/errors-are-values)
- `string`
    - Hinweise auf UTF8-Encoding
    - siehe auch [package unicode/utf8](https://golang.org/pkg/unicode/utf8/)
    - [package golang.org/x/text](https://godoc.org/golang.org/x/text) für andere Encodings
    - [golang.org/x/text/encoding/charmap](https://godoc.org/golang.org/x/text/encoding/charmap)
    - [Beispiel intests / document.go](https://bitbucket.org/s_l_teichmann/intests/src/9e39252ea071feaa101c570f66aee6502caa65bf/document.go?at=default&fileviewer=file-view-default#document.go-343)

## Tag 2

### II - Typsystem

- Typen
- Methoden, Method-Sets
- Reciever, Pointer-Reciever
- Interfaces, implizites Erfüllen
    - [implizites Erfüllen am Beispiel type Stringer im package fmt](https://golang.org/pkg/fmt/#Stringer)
- Embedding (Exkurs Objektorientierung mit anderen Mitteln)
    - Komposit-Gedanke
    - Vererbung mittels Embedding
- Kontrollstrukturen III:
    - Typ-Zusicherungen mittels Type-Assertions
    - type switch
      - [Beispiel aus dem Package fmt](https://golang.org/src/fmt/print.go#L623)
- Funktionen III:
    - Method-Values
      - [Bsp. intests/controller.go](https://bitbucket.org/s_l_teichmann/intests/src/9e39252ea071feaa101c570f66aee6502caa65bf/controller.go?at=default&fileviewer=file-view-default#controller.go-42)
- Method-Expressions
    - [Bsp. go-examples/methodexp/main.go](https://bitbucket.org/s_l_teichmann/go-examples/src/d95e82c4bfcfe077a6a4fe8a621e526d5bf41fc0/methodexp/main.go?fileviewer=file-view-default#main.go-106)

- Konstanten `const`: typsiert, untypisiert
    - [Bsp. time.go in package time](https://golang.org/pkg/time/#Duration)
    - [Iota](https://golang.org/ref/spec#Iota)

### III - Standard-Bibliothek (optional)

- `io`, `fmt`, `os`, ...

### IV - Concurrency (Nebenläufigkeit)

- Einleitung
    - [Communicating Sequential Processes (CSP) von Tony Hoare](https://de.wikipedia.org/wiki/Communicating_Sequential_Processes)
    - [Concurrency is not parallelism (Rob Pike)](https://blog.golang.org/concurrency-is-not-parallelism)
    - [C10k problem](https://en.wikipedia.org/wiki/C10k_problem)
- Go-Routines `go f()`
- Channels `chan`
    - [Effective Go / channels](https://golang.org/doc/effective_go.html#channels)
- Kontrollstrukturen IV:
    - `range`, `<-`, `->`,
    - `select`
      - [go-examples/goroutineleakfixed/main.go](https://bitbucket.org/s_l_teichmann/go-examples/src/d95e82c4bfcfe077a6a4fe8a621e526d5bf41fc0/goroutineleakfixed/main.go?at=default&fileviewer=file-view-default)
- Memory-Model
- Orchestrierend vs. serialisierend (`sync`-Paket)
    - Orchestrierend
        - Channels `chan`
    - serialisierend
        - waitGroup
        - Mutex
        - atomic
- Race-Detector

## Tag 3

- struct-Tags

### Go - Tooling

- `go fmt`, `go vet`, `go lint`, ...
    - [Go tooling essentials (Jaana Burcu Dogan)](http://rakyll.org/go-tool-flags/)
- `guru`
    - [Using Go Guru: an editor-integrated tool for navigating Go code](https://docs.google.com/document/d/1_Y9xCEMj5S-7rv2ooHpZNH15JgRT5iM742gJkw5LtmQ/edit#heading=h.ojv16z1d1gas)
- `go test` Unit-Tests und Coverage
- Benchmarking, Profiling
    - [Profiling & Optimizing in Go (Brad Fitzpatrick)](https://www.youtube.com/watch?v=xxDZuPEgbBU)
- Tracer
- [Debugging](debugging.md)
- [Entwicklungsumgebungen](editoren_ide.md)

### V: Bauen (optional)

- Build Tags
- CGo: Interaktion mit C-Bibliotheken
- Build Modi
- Cross-Compiling
- Vendoring

### VI: Meta-Programming (optional)

- Reflection: Types, Values (Hinweis: Konzept der Pointer in Go muss verstanden sein!!!)
- AST-Repräsentierung
- Generierung von Code: `go generate`
    - Beispiel: [https://github.com/s-l-teichmann/genmap](https://github.com/s-l-teichmann/genmap)
- interne Data-Repräsentierung
- `unsafe`
