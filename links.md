# Wichtige Links

## Project

* [golang.org](https://golang.org/)
* [golang.org/dl](https://golang.org/dl)
* [Documents / How to write Go code](https://golang.org/doc/code.html)
* [Documents / Effective Go](https://golang.org/doc/effective_go.html)
* [Documents / FAQ](https://golang.org/doc/faq)
* [API Doc Standard Library](https://golang.org/pkg/)
* [GoDoc - Index](https://godoc.org/)

## Community

* [Github - Issue Tracking etc.](https://github.com/golang/)
* [Anwender - Nuts](https://groups.google.com/forum/#!forum/golang-nuts)
* [Core Entwicklung](https://groups.google.com/forum/#!forum/golang-dev)
* [Code reviews](https://groups.google.com/forum/#!forum/golang-codereviews)
* [Go Forum](https://forum.golangbridge.org/)

## Buchempfehlung: The Go Programming Language

* [The Go Programming Language](http://www.gopl.io/)
* [Example programs from "The Go Programming Language"](https://github.com/adonovan/gopl.io/)

## Mehr Kontext

* [Less is exponentially more (Rob Pike)](https://commandcenter.blogspot.de/2012/06/less-is-exponentially-more.html)
* [Five things that make Go fast (Dave Cheney)](http://dave.cheney.net/2014/06/07/five-things-that-make-go-fast)
* [Stupid Gopher Tricks (Andrew Gerrand)](https://talks.golang.org/2015/tricks.slide)

## Motivation

* [Building Street Address Autocomplete with Go](https://blog.gopheracademy.com/birthday-bash-2014/building-street-address-autocomplete/)
* [Imposm is an importer for OpenStreetMap data. Performance boost after migration to go.](https://github.com/omniscale/imposm3#performance)
* [HOW WE BUILT UBER ENGINEERING�S HIGHEST QUERY PER SECOND SERVICE USING GO](https://eng.uber.com/go-geofence/)
* [Pointstream is a little system to demonstrate the usage of Server Sent Events to do real-time broadcasting in a web GIS application](https://bitbucket.org/s_l_teichmann/pointstream)
* [An interpretation of of Radomir 'The Sheep' Dopieralski's Z-Day](https://bitbucket.org/s_l_teichmann/zday)