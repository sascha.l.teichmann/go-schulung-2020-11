package main

import "fmt"

func good() *int {
	var x int
	return &x
}

func foo(a *int) float32 {
	fmt.Println(a)
	*a = 32
	return 3.1415
}

func main() {
	// int uint int8 uint8 int16 uint16 int32 uint32 int64 uint64
	// float32 float64 bool rune string byte
	// uintpr complex64 complex128

	var x float32
	y := 11
	x = foo(&y)

	var f float64

	y := byte(x)

	_ = f
}
