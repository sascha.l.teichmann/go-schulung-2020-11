package main

func main() {

	// +, -, *, /, ++, --, +=, *=, /=, %, %=,
	// && || == != ^

	var cond bool

	if x := f(); x == 2 {
		// x
	} else if y := f(); y > 3 {
		// x, y
	} else if blau {
		// x, y
	} else {
		// x, y
	}

	for ; x < 10; x++ {
	}

foo:
	for {
		// ...
		if cond {
			break foo // continue
		}
	}

	for {
		goto out
	}

out:

	switch x := f(); x {
	case 1, 2:
		//
	default:
	}

	switch {
	case a < 10:
		//
		fallthrough
	case b < 100:
		//
	default:
	}
}
