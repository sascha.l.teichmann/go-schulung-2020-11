package main

import "fmt"

func foo(x int) {
	fmt.Println(x)
}

func bar(f func(int)) {
	f()
}

func inc() func() int {
	var i int
	return func() (x int) {
		x = i
		i++
		return
	}
}

func main() {
	f := foo
	f()
	bar(foo)
	bar(f)
	i := inc()
	i()
	i()
	i()

}
