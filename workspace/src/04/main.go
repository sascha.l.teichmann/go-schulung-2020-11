package main

import (
	"fmt"
	"log"
	"regexp"
)

var digit = regexp.MustCompile("(\\d+)")

func swap(a, b int) (int, int) {
	return b, a
}

func may(a int) (result int, err error) {

	defer func() {
		if p := recover(); p != nil {
			err = fmt.Errorf("wrong: %v\n", p)
		}
	}()

	defer fmt.Println("defer 2")

	panic("Boom!")

	fmt.Println("Success")

	return
}

func main() {

	if _, err := may(19); err != nil {
		log.Printf("error: %v\n", err)
	}
	fmt.Println("After")
	//p := recover()
}
