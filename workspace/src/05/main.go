package main

import (
	"fmt"
	"log"
)

func main() {

	var err error

	write := func(s string) {
		if err != nil {
			_, err = fmt.Println("foo")
		}
	}

	write("foo")
	write("bar")
	write("baz")

	if err != nil {
		log.Printf("err: %v\n", err)
	}
}
