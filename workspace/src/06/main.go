package main

import "fmt"

func foo(
	x *[2]int,
) {
	x[1] = 42
}

var baa [2]int

func main() {
	var arr [2]int

	arr[0] = 1
	arr[1] = 2

	foo(&[2]int{
		1: 2,
		0: 33,
	})

	foo(&arr)
	fmt.Println(arr[1])

}
