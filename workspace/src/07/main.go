package main

import "fmt"

func main() {

	var arr [10]int

	arr[1] = 42
	arr[2] = 43

	s := arr[1:3]

	fmt.Println(s[0])
	fmt.Println(s[1])
	fmt.Println(len(s))
	fmt.Println(cap(s))

	s2 := s[:1]
	fmt.Println(len(s2))
	fmt.Println(cap(s2))

	arr[1] = 33
	fmt.Println(s2[0])

	for i := range s {
		s[i] = 666
		fmt.Printf("-- %d\n", s[i])
	}

}
