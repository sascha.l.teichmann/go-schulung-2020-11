package main

import "fmt"

func foo(x []int) {
	x = append(x, 10)
	fmt.Println(x)
}

func main() {
	s := make([]int, 0, 6)
	fmt.Println(len(s))
	fmt.Println(cap(s))

	s = append(s, 10, 10, 10, 10, 10, 10)
	foo(s)
	fmt.Println(s)

	pl := make([]*[10000]byte, 10)

	func() {
		var x [10000]byte
		pl[9] = &x
	}()

	pl[9] = nil
	pl = pl[:len(pl)-1]
	fmt.Println(pl)
	pl = pl[:len(pl)+1]
	fmt.Println(pl)

}
