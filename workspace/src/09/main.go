package main

import "fmt"

func variadic(x int, y ...int) {

	for i, v := range y {
		fmt.Printf("%d: %d\n", i, v)
	}
}

func main() {

	x := 10

	variadic(x)
	variadic(x, []int{42, 43}...)
}
