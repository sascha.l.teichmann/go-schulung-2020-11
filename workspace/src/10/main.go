package main

type myStruct struct {
	bar rune
	v   complex128
}

func f(ms *myStruct) {
	ms.bar = 'Ü'
}

func main() {

	type myStructX struct {
		bar rune
		v   complex128
	}

	var x = struct {
		a float64
		b int
		_ bool
		c string

		sub struct {
			foo int
		}
	}{
		c: "Hello",
	}

	c = 1

	x.a = 3.14
	x.b = 42
	x.sub.foo = 22
}
