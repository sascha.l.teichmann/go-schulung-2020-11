package main

import "fmt"

func main() {

	type key struct {
		x int
		y int
		_ func()
	}

	coords := map[key]string{
		{61, 72, func() {}}: "Bielefeld",
	}

	_ = coords

	var m map[string]int

	m = make(map[string]int)

	m["tom"] = 42
	m["ben"] = 100

	p := m["alex"]

	fmt.Println(m["tom"], p)

	_, ok := m["tom"]
	fmt.Println(ok)

	for k, v := range m {
		fmt.Printf("%s: %d\n", k, v)
	}

}
