package main

import "fmt"

type foo int

func (f foo) print() {
	fmt.Printf("print: %d\n", f)
}

func (f *foo) zero() {
	*f = 0
}

func do(fn func()) {
	fmt.Println("do")
	fn()
	fmt.Println("done")
}

func main() {

	var x foo

	x = 1

	var y int = 2

	x = foo(y)

	x.print()

	(&x).zero()

	x.print()

	do(func() { x.print() })

	do(x.print)
}
