package main

import "fmt"

type myConst int

const (
	a = myConst(1 << iota)
	b
	c
)

func (mc myConst) Foo() {
	fmt.Println("foo")
}

func main() {
	const pi = 1.1415

	var x int = 0.4 + 0.6
	_ = x

	f := pi

	fmt.Println(f)
	fmt.Println(c)
	c.Foo()

	//var s int = "Hello"
}
