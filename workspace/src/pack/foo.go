package pack

import "fmt"

var X int = func() int {
	return 4
}()

func init() {
	fmt.Println("init")
}

func init() {
	fmt.Println("init2")
}

func foo() {
	fmt.Println("foo")
}

func Foo() {
	foo()
}
